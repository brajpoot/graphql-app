# RubyOnRails Setup

`gem install bundler`

`gem install rails -v 6`

`rails new graphql-tutorial --skip-action-mailer --skip-action-mailbox --skip-action-text --skip-active-storage --skip-action-cable --skip-javascript --skip-system-test --skip-webpack-install`

`cd graphql-tutorial`

`bundle exec rails db:create`

`bundle exec rails server`


#Open Gemfile and add the following dependency to it:

`gem 'graphql', '1.9.17'`


`bundle install`

`bundle exec rails generate graphql:install`

#Open Gemfile and change the following line:

`gem 'graphiql-rails', group: :development`
to

`gem 'graphiql-rails', '1.7.0', group: :development`

`bundle install`

`bundle exec rails generate model Link url:string description:text`

`bundle exec rails db:migrate`

#Run rails console and then create a couple of dummy Links:

`Link.create url: 'http://graphql.org/', description: 'The Best Query Language'`

`Link.create url: 'http://dev.apollodata.com/', description: 'Awesome GraphQL Client'`
`exit`

`rails g graphql:object LinkType id:ID! url:String! description:String!`

#When you previously ran rails generate graphql:install, it created the root query type in app/graphql/types/query_type.rb for you.
```
module Types
  class QueryType < BaseObject
    # queries are just represented as fields
    # `all_links` is automatically camelcased to `allLinks`
    field :all_links, [LinkType], null: false

    # this method is invoked, when `all_link` fields is being resolved
    def all_links
      Link.all
    end
  end
end
```


#/app/assets/config/manifest.js

`//= link graphiql/rails/application.css`

`//= link graphiql/rails/application.js`


#HelpfulLinks

https://graphql.org/code/

https://www.howtographql.com/graphql-ruby/1-getting-started/

