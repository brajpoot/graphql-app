# frozen_string_literal: true

module Base
  class BaseEnum < GraphQL::Schema::Enum
  end
end
