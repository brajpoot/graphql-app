# frozen_string_literal: true

module Enums
  class StatusEnum < Base::BaseEnum
    value 'PUBLIC', value: 'public'
    value 'PRIVATE', value: 'private'
  end
end
