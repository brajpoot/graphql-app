# frozen_string_literal: true

module Mutations
  class CreateLink < BaseMutation
    # arguments passed to the `resolve` method
    argument :description, String, required: true
    argument :url, String, required: true
    argument :status, Enums::StatusEnum, required: false
    argument :user, ID, required: false

    field :link, Types::LinkType, null: false
    field :errors, [String], null: false

    def resolve(args = {})
      link = Link.create(args)
      { link: link, errors: [] }
    rescue ActiveRecord::RecordInvalid => e
      { link: link, errors: [e.full_messages.join(', ')] }
    end
  end
end
