# frozen_string_literal: true

module Mutations
  class CreateUser < BaseMutation
    # often we will need input types for specific mutation
    # in those cases we can define those input types in the mutation class itself
    class AuthProviderSignupData < Base::BaseInputObject
      argument :credentials, Types::AuthProviderCredentialsInput, required: false
    end

    argument :name, String, required: true
    argument :auth_provider, AuthProviderSignupData, required: false

    field :user, Types::UserType, null: false
    field :errors, [String], null: false

    def resolve(name: nil, auth_provider: nil)
      user = User.create!(name: name,
                          email: auth_provider&.[](:credentials)&.[](:email),
                          password: auth_provider&.[](:credentials)&.[](:password))
      { user: user, errors: [] }
    rescue StandardError => e
      { user: nil, errors: [e.full_messages.join(', ')] }
    end
  end
end
