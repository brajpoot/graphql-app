# frozen_string_literal: true

module Types
  class LinkType < Base::BaseObject
    field :id, ID, null: false
    field :url, String, null: false
    field :description, String, null: false
    field :posted_by, UserType, null: true, method: :user
    field :votes, [Types::VoteType], null: false
    field :status, Unions::StatusUnion, null: false

    def status
      object
    end
  end
end
