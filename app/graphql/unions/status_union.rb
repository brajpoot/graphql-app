# frozen_string_literal: true

module Unions
  class StatusUnion < Base::BaseUnion
    possible_types Types::UserType, Types::LinkType

    def self.resolve_type(object, _context)
      case object.status
      when 'public' then Types::LinkType
      when 'private' then Types::UserType
      end
    end
  end
end
